// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{	
	// Prueba practica3
	//pdatos -> accion=10;
	//munmap( pdatos,sizeof(DatosMemCompartida) );
	//unlink ("/tmp/datos");
	
	//Practica 4
	close(fdfifo_posicion);
	unlink("/tmp/tuberia_posicion");
	close (fdfifo_teclas);
	unlink("/tmp/tuberia_teclas");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	char cad[200];
	read(fdfifo_posicion, cad, sizeof(char) * 200);
	sscanf(cad, "%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x, &esfera.centro.y,
&jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2,
&jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2,
&puntos1,&puntos2);
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	
	//Nuevo practica3
	/*

	datos = *pdatos;
	pdatos -> esfera = esfera;
	pdatos -> raqueta1 = jugador1;

	if( pdatos -> accion == 1)
		OnKeyboardDown( 'w', 0,0);
	else if (pdatos -> accion == -1 )
		OnKeyboardDown( 's', 0,0 );
	*/	
	//

	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
	}
	
	//Nuevo practica3
	
	char mensaje[100];
	sprintf( mensaje, "Jugador 2 marca 1 punto, lleva un total de %d puntos.", puntos2);
	write( fdfifo, mensaje, sizeof(char)*100);
	
	if( puntos2 >= 3) exit(0);

	//

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	}

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{	
	//NUEVO practica 3
	/*
	if ((fdfifo = open("tuberia", O_RDONLY))<0) {
		perror( "No se puede abrir el fifo");
		exit(0);
	}

	if( ( fddatos = open ("datos", O_RDWR | O_CREAT | O_TRUNC, 0600 ))<0) {
		perror( "Error al abrir el fichero para proyeccion en memoria");
		exit(0);
	}
	
	else {
		write(fddatos, &datos, sizeof(datos));
		pdatos = (DatosMemCompartida*) mmap(NULL,siceof(datos, PROT_WRITE | PROT_READ, MAP_SHARED, fddatos, 0);
		close(fddatos);
	}
	
	pdatos -> accion = 0;
	*/	
	//


	//Nuevo Practica4
	
	//Creamos la fifo de posiciones
	error = mkfifo("/tmp/tuberia_posicion",0600);
	if( errno != EEXIST && error <0) {
	perror("No se ha podido crear el FIFO de posicion");
	exit(0);
	}

	//Creamos la fifo de teclas
	error = mkfifo("/tmp/tuberia_teclas",0600);
	if( errno != EEXIST && error <0) {
	perror("No se ha podido crear el FIFO de teclas");
	exit(0);
	}

	// Abrimos el fifo de posiciones
	fdfifo_posicion = open("/tmp/tuberia_posicion", O_RDONLY);
	if(fdfifo_posicion<0) {
	perror("No se ha podido abrir el FIFO de posicion");
	exit(0);
	}
	
	// Abrimos el fifo de teclas
	fdfifo_teclas = open("/tmp/tuberia_teclas", O_WRONLY);
	if(fdfifo_teclas<0) {
	perror("No se ha podido abrir el FIFO de teclas");
	exit(0);
	}



	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
