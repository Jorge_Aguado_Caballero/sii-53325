#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>

#define BUFFER 55


int main()
{
	int fd, error, lectura;
	
	char mensaje [BUFFER];
	
	error =mkfifo("tmp/tuberia", 0600);
	if(errno != EEXIST && error <0) {
		perror("No se puede crear la tuberia");
		return(-1);	
	}
	
	fd = open( "tmp/tuberia", ORD_ONLY);
	if(fd<0) {
	perror("No se puede abrir la tuberia");
	return(-1);	
	}
	
	error = 1;
	while (error > 0) {
	
	lectura = read( fd, mensaje, sizeof(char) * BUFFER);

	printf("%s\n", mensaje);

	if(lectura < 0)
	{
	perror("No se puede leer la tuberia");
	return(-1);	
	}
		
	else if ( lectura == 0){
	error =-1;

	}

	}
	
	close(fd);
	unlink("tmp/tuberia");

	retunr 0;
}
